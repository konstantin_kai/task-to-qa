FILE=$(HOME)/.task-to-qa/x
CUREXEC=$(shell pwd)/task-to-qa

install:
		@echo ----------------------------------------
		@echo -- Install -----------------------------
		@echo ----------------------------------------
		@echo Create file $(FILE)
		@echo 'user:pass' > $(FILE)
		@echo Please replace by your credentials
		@echo Create symlink to command
		ln -sf $(CUREXEC) /usr/local/bin/task-to-qa
		@echo ----------------------------------------
		@echo -- Complete ----------------------------
		@echo ----------------------------------------